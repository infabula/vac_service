from flask import jsonify
from flask import Flask
from flask import render_template, abort
from flask_restful import Resource, Api, request
from flask_restful import reqparse


db = { "vaccins": {
           'phizer': {'name': 'Phizer', 'count': 2},
           'moderna': {'name': 'Moderna', 'count': 2},
           'spoetnic': { 'name': 'Spoetnic', 'count': 1}
     }
}

app = Flask(__name__)  # HTML
api = Api(app)  # REST API

# HTML afdeling

@app.route('/')  # decorator
def show_index():
    return render_template('index.html', name="Trainee")

@app.route('/hello')  # decorator
def hello_world():
    return 'Hello, World!'

@app.route('/vaccin')  # decoratorstapeling
@app.route('/vaccins')  # decorator
def show_vaccins():
    content = "<h1>Vaccins</h1>"
    content += "<ul>"
    for vaccin in db['vaccins']:
        props = db['vaccins'][vaccin]

        content += '<li><a href="/vaccins/{id}">{name}</a></li>'.format(id=vaccin,
                                                                         name=props['name'])
    content += "</ul>"
    return content


@app.route('/vaccins/<string:vaccin_id>')
def show_vacin_details(vaccin_id):
    vaccin_name = vaccin_id.lower()
    if vaccin_name in db['vaccins']:
        props = db['vaccins'][vaccin_name]
        return "Info over " + props['name']
    else:
        return "Vaccin is niet gevonden"

@app.errorhandler(404)
def not_found(error):
    return render_template('error.html'), 404



# REST API
@app.route('/api/v1/vaccins/<string:vaccin_id>', methods=['GET'])
def get_vaccin_details(vaccin_id):
    vaccin_name = vaccin_id.lower()
    if vaccin_name in db['vaccins']:
        props = db['vaccins'][vaccin_name]
        return jsonify(props)
    else:
        abort(404)

class Vaccin(Resource):
    def get(self, vaccin_id):
        vaccin_name = vaccin_id.lower()
        if vaccin_name in db['vaccins']:
            props = db['vaccins'][vaccin_name]
            return props
        else:
            abort(404)

    def post(self, vaccin_id):
        print("POST op Vaccin")
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help="Name of the vaccin")
        parser.add_argument('count', type=int, help="prik-count")
        args = parser.parse_args(strict=True)

        db["vaccins"][args["name"].lower()] = { 'name': args['name'],
                                                'count': args["count"]}
        return db["vaccins"][args["name"].lower()]

api.add_resource(Vaccin, '/api/v2/vaccins/<string:vaccin_id>')