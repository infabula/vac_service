from setuptools import setup, find_packages


setup(
    name='vac_service',
    version='0.0.1',
    url='https://example.org',
    author='Jeroen',
    author_email='anonymous@example.org',
    packages=['vac_service']
)